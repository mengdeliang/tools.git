<?php
namespace Muction\Tools\Builder\Constructor\Lib;

use Muction\Tools\Exception\BuilderException;
use Muction\Tools\Builder\Constructor\Lib\InterfaceDatabase;

abstract class AbstractDatabase implements InterfaceDatabase {

	/**
	 * 数据库
	 * @var string
	 */
	public $database='';

	/**
	 * 表
	 * @var string
	 */
	public $table='';

	/**
	 * 字段
	 * @var array
	 */
	protected $column=[];


	/**
	 * 返回SQL
	 * @var string
	 */
	public $sql='';

	public function createDatabase($databaseName='testDatabase'){}
	public function dropDatabase($databaseName='testDatabase'){}
	public function createTable($tableName='testTable'){}
	public function alterTable($tableName='testTable'){}
	public function dropTable($tableName='testTable'){}
	public function lastSql(){
		return $this->sql;
	}

	protected function fields(array $options){}




}