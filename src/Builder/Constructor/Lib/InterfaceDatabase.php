<?php

namespace Muction\Tools\Builder\Constructor\Lib;

interface InterfaceDatabase{

	const VERSION=1.0;
	public function createDatabase($databaseName='testDatabase');
	public function dropDatabase($databaseName='');
	public function createTable($tableName='testTable');
	public function alterTable($tableName='testTable');
	public function dropTable($tableName='testTable');
	public function lastSql();

}