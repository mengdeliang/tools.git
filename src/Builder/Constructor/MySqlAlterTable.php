<?php
namespace Muction\Tools\Builder\Constructor;

class MySqlAlterTable {

	private $object=null;

	static private $instance;

	public function __construct($object)
	{
		$this->object=$object;
	}

	public static function getInstance($obj){
		if(! self::$instance instanceof self){
			self::$instance=new self($obj);
		}
		return self::$instance;
	}

	public function changeTableEngine($engine){
		$this->object->sql = "ALTER TABLE `{$this->object->table}` ENGINE={$engine}";
	}

	/**
	 * @param $newName
	 */
	public function renameTable($newName){
		$this->object->sql = "ALTER TABLE `{$this->object->table}` RENAME `{$newName}`";
	}

	/**
	 * @param $oldName
	 * @param $newName
	 * @param string $dataType
	 */
	public function renameColumn($oldName,$newName,$dataType=''){
		$this->object->sql = "ALTER TABLE `{$this->object->table}` CHANGE `{$oldName}` `{$newName}` {$dataType}";
	}

	/**
	 * @param $column
	 * @param $type
	 */
	public function modifyColumnType($column,$type){
		$this->object->sql= "ALTER TABLE `{$this->object->table}` MODIFY `{$column}` {$type}";
	}


	/**
	 * @param $column
	 * @param $type
	 * @param string $constraint FIRST|AFTER
	 * @param $column2
	 */
	public function addColumn($column,$type,$constraint='',$column2=''){
		$column2=$column2? "`{$column2}`" : '';
		$this->object->sql = "ALTER TABLE `{$this->object->table}` ADD `{$column}` {$type} {$constraint} {$column2}";
	}

	/**
	 * @param $column
	 */
	public function dropColumn($column){
		$this->object->sql = "ALTER TABLE `{$this->object->table}` DROP `{$column}`";
	}

	/**
	 * @param $column1
	 * @param $type1
	 * @param $sort AFTER|FIRST
	 * @param $column2
	 */
	public function sortColumn($column1,$type1,$sort,$column2){

		$this->object->sql = "ALTER TABLE `{$this->object->table}` MODIFY `{$column1}` {$type1} {$sort} `{$column2}`";
	}
}