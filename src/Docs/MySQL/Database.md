## 数据库的基本操作

> 登录数据库

	mysql -h -u -p

> 查看所有数据库

	SHOW DATABASES

> 创建数据库

	CREATE DATABASE `database_name`
	
> 查看数据库创建定义

	SHOW CREATE DATABASE `database_name`	
	
> 删除数据库

	DROP DATABASE `database_name`	
	
## 数据库存储引擎

> DBMS 
	
	数据库管理系统
	
> 查看当前数据库支持引擎

	SHOW ENGINES
	
> InnoDB 存储引擎特性（是事务性数据库首选引擎、为处理巨大数据量的最大性能设计）
	
    1. InnoDB提供MySQL具有提交，回滚，和崩溃回复能力的事务安全存储引擎。InnoDB锁定在行级并且也在 SELECT 语句中提供类似 Oracle的非索
        定读。可以将InnoDB 类型的表与其他MySQL的表的类型混合起来。
     
    2. InnoDB是为处理巨大数据量的最大性能设计。他的CPU效率可能是任何其他基于磁盘的关系数据库引擎所不能匹敌的。
    
    3. InnoDB与MySQL服务器整合，InnoDB存储引擎为在主内存中缓存数据和索引维持他自己的缓冲池。InnoDB将他的表和索引存在一个逻辑表空间中
       表空间可以包含数个文件（或原始磁盘分区）。MyISAM 表不同，MyISAM表中的每个表被存在分离的文件中。InnoDB表可以是任何尺寸，即使文件
       尺寸被限制为2GB的操作系统上。    
    
    4. InnoDB支持外键完整性约束(FOREIGN KEY)，存储数据时，每张表的存储都按主键顺序存放，如果没有主键，InnoDB会每一行生成一个6B的ROWID
       并以此为主键。
    
    5. InnoDB被用在众多需要高性能的大型数据库站点上。
       InnoDB 不创建目录，使用InnoDB时，MySQL将在MySQL数据目录下创建一个ibdata1 的10MB大小的自动扩展数据文件，以及两个名为 ib_logfile0
       ib_logfile1 的5MB大小的日志文件 。

> MyISAM 存储引擎

	 MyISAM基于ISAM的存储引擎，并对其进行扩展。他是在 Web、数据存储和其他应用环境下最常用的催出引擎。MyISAM拥有叫的插入、查询速度。
	   但不支持事务。在 MySQL5.5.5之前版本中，MyISAM 为默认存储引擎，主要特性如下：
	 
	 1. 大文件（达63位文件长度） 在支持大文件的文件系统和操作系统上被支持。
	 2. 当把删除、更新及插入操作混合使用时，动态尺寸的行产生更少碎片。这要通过合并相邻被删除的块，以及若下一个块被删除、就扩展到下一个块自动完成
	 3. 每个MyISAM 表最大索引数是 64，这可以通过重新编译来改变。每个索引最大的列数是16个。
	 4. 最大的建长度是1000B，也可以通过重新编译来解决。
	 5. BLOB 和 TEXT 列可以被索引。
	 6. NULL 值被允许在索引列中
	 7. 所有的数字键值以高字节悠闲被存储以允许一个更高的索引压缩。
	 
	 使用MyISAM存储引擎，可产生3个文件  table_name.frm 文件存储表定义  table_name.MYD（MYData）数据文件  table_name.MYI 索引文件
	 
> MEMORY 存储引擎
	
	 MEMORY 存储引擎将表中的数据存储在内存中，为查询和索引其他表数据提供快速访问。特性：
	 
	 1. MEMORY 每个表可以有多大32个索引，每个索引有16列，以及 500B的最大键长度。
	 2. MEMORY 存储引擎执行 HASH 和BTREE索引
	 3. 可以在一个MEMORY表中有非唯一键
	 4. MEMORY 表使用一个固定的记录长度格式。
	 5. MEMORY 不支持 BLOB 和TEXT列
	 6. MEMORY 支持 AUTO_INCREMENT 列和对可包含 NULL值的列的索引。
	 7. MEMORY 表在所有客户端直接共享
	 8. MEMORY 表内容被存在内存中，内存是MEMORY表和服务器查询处理时空闲中创建的内部表共享。
	 9. 当不需要MEMORY表的内容时，要释放被 MEMORY 表使用的内存，应该执行 DELETE FROM 或 TRUNCATE TABLE 或者删除整个表 DROP TABLE 
	 	 	                
> 存储引擎选择，建议如下：
	 
	 1. InnoDB 事务，并发控制
	 2. MyISAM 主要用于是插入查询记录
	 3. Memory 临时存放数据，数据量不大
	 4. Archive 支持高并发的插入操作，适合存储归档数据，如记录日志信息   				