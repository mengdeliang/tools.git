<?php

require '../../vendor/autoload.php';

use Muction\Tools\Builder\Constructor\MySql;


$fields=[
	[
		//'length'=>11,
		'name'=>'id',
		'type'=>'Int',
		'comment1'=>'dasd',
		'primary'=>true,
		'index'=>true,
		'increment'=>true,
		'unsigned'=>true,
		'null'=>true,
		'default'=>'11',
		'charset'=>'utf8',
		'collate'=>'utf8_general_ci'
	],
	[
		'name'=>'Category',
		'type'=>'varchar',
		'length'=>120,
		'default'=>'caasd',
		'comment'=>'新闻分类',
		'charset'=>'utf8',
		'collate'=>'utf8_general_ci',
		'primary'=>true,
		'null'=>true,

	]
];

$table = [
	'name'=>'news','engine'=>'MYISAM','charset'=>'utf8','collate'=>'utf8_general_ci'
];

$mysql = new MySql();

try{
	$mysql=MySql::getInstance();
	$mysql->createTable($table)->column($fields);
	$mysql->createDatabase('Test');
	$mysql->alterTable('news')->renameTable("ddd");
	$mysql->alterTable('category')->addColumn("newColumn","varchar(214)" , "AFTER","id");
	$mysql->alterTable("comment")->modifyColumnType("category","char(100)");
	$mysql->alterTable('ddd')->dropColumn('id');
	$mysql->alterTable('ddd')->sortColumn('category','varchar(100)',"AFTER" , "id");
	echo $mysql->lastSql();

}catch (Exception $e){
	echo $e->getMessage();
}






