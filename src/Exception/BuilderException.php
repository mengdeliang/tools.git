<?php

namespace Muction\Tools\Exception;

/**
 * todo 完善异常类
 * Class BuilderException
 * @package Muction\Tools\Exception
 */
class BuilderException extends \Exception{

	public static function invalidArgument($arg){
		return new static($arg);
	}

	public function __toString() {
		$message="<H2>{$this->message}</H2>";
		//$message.="<H4>{$this->code}</H4>";
		return $message;
		//return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}