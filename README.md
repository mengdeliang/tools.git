Muction Tools For Builder5
===============


Tools主要为Builder网站后台提供服务，目前Tools功能主要包括：

 + MySQL语句构建器（不真实操作数据库，只用来生成SQL语句）

### MySQL语句构造器

>字段配置项

	name    字段名称
	length  字段类型长度
	type    字段类型（可选值参考下列字段类型）
	comment 备注
	charset 字符集
	collate 字符集
	notnull 不允许为空，可选值 Bool
	null    允许为空，可选值 Bool
	default 默认值
	increment 是否自增，可选值 Bool
	index   索引
	primary 主键
	unsigned 非负数
		
>表名配置项
	
	name    表名称
	engine  存储引擎（可选值参考下列存储引擎）
	charset 字符集
	collate	字符集
		
>存储引擎

	INNODB
	MYISAM
	MEMROY
	CSV
	FEDERATED
	ARCHIVE
	BLACKHOLE	
	
>字段类型(常用)

	CHAR 固定长度非二进制字符串 1~255个字符 用空格右侧填充补全
	VARCHAR 边长非二进制字符串 0~65535个字符 L+1字节 L<=M 和 1<=M<=255
	TINYTEXT非常小的非二进制字符串 L+1 L<2的8次方
	TEXT小的非二进制字符串 L+2 L<+2的16次方
	MEDIUMTEXT中等大小的非二进制字符串 L+3 L<=2的24次方
	LONGTEXT非二进制字符串 太大了不知道怎么表示了
	
	TINYINT  1个字节  -128~127 0~255
	SMALLINT 2个字节 -32768~32767 0~65535
	MEDIUMINT  3个字节 -8388608~8388607 0~16777215
	INT 4个字节 integer -2147483648~2147463647 0~4294967295 40亿
	BIGINT8个字节 -922337203685477588~9223372036854775807 0~18446744073709551615
	FLOAT  4个字节单精度
	DOUBLE 8个字节双精度
	DECIMALDECIMAL为定点小数 (MN) m表示精度，n表示标度 ，定点数以字符串形式存储 。货币，科学数据推荐此数据类型
	YEAR 1个字节 1901-2155
	TIME3个字节
	DATE 3个字节
	DATETIME 8个字节
	TIMESTAMP 4个字节
	
	BINARY 
	VARBINARY 
	BLOB 
	ENUM 枚举类型，只能有一个枚举字符串值 1或2个字节 取决于枚举值的数目最大 65535 。值内部用整数表示每个枚举值均有一个索引值从1开始编号
	SET 一个设置，字符串对象可以有零个或多个SET成员 12348 最多64个成员 值内部用整数表示
	
	BIT位字段类型 1~64默认为1
	BINARY固定长度二进制字符串 类似于CHAR
	VARBINARY可变长度二进制字符串 类似于VARCHAR
	
	TINYBLOB非常小的BLOB
	BLOB小BLOB 存储是二进制字符串（字节字符串） 没有字符集
	MEDIUMBLOB中等大小BLOB
	LONGBLOB非常大的BLOB	

>例子
	
	require '../../vendor/autoload.php';
    use Muction\Tools\Builder\Constructor\MySql;
    
	$fields=[
		[
			'length'=>11,
			'name'=>'id',
			'type'=>'Int',
			'comment1'=>'dasd',
			'primary'=>true,
			'index'=>true,
			'increment'=>true,
			'unsigned'=>true,
			'null'=>true,
			'default'=>'11',
			'charset'=>'utf8',
			'collate'=>'utf8_general_ci'
		],
		[
			'name'=>'Category',
			'type'=>'varchar',
			'length'=>120,
			'default'=>'caasd',
			'comment'=>'新闻分类',
			'charset'=>'utf8',
			'collate'=>'utf8_general_ci',
			'primary'=>true,
			'null'=>true,
	
		]
	];
	
	$table = [
		'name'=>'news','engine'=>'MYISAM','charset'=>'utf8','collate'=>'utf8_general_ci'
	];
	
	$mysql = new MySql();
    
    try{
    	$mysql=MySql::getInstance();
    	
    	//创建表
    	$mysql->createTable($table)->column($fields);
    	//创建数据库
    	$mysql->createDatabase('00');
    	//重命名表
    	$mysql->alterTable('news')->renameTable("ddd");
    	//添加字段
    	$mysql->alterTable('category')->addColumn("newColumn","varchar(214)" , "AFTER","id");
    	//修改字段类型
    	$mysql->alterTable("comment")->modifyColumnType("category","char(100)");
    	//删除字段
    	$mysql->alterTable('ddd')->dropColumn('id');
    	//字段排序
    	$mysql->alterTable('ddd')->sortColumn('category','varchar(100)',"AFTER" , "id");
    	//获取最后一条SQL语句
    	echo $mysql->lastSql();
    
    }catch (Exception $e){
    	echo $e->getMessage();
    }